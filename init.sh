#!/usr/bin/env bash
# source activate meshroom
pip install -e ./third_party/Hierarchical-Localization/
# compile the C++ codes
mkdir ./src/utils/srcRenderDepth/build
cd ./src/utils/srcRenderDepth/build
cmake -Dpybind11_DIR=/usr/local/lib/python3.8/dist-packages/pybind11/share/cmake/pybind11/ ..
cmake -D Python_EXECUTABLE=$(which python) ..
cmake --build . --config Release --target install
echo "RenderDepth done"

cd ../../../..
mkdir ./src/meshroom/MeshroomCpp/build
cd ./src/meshroom/MeshroomCpp/build
cmake -Dpybind11_DIR=/usr/local/lib/python3.8/dist-packages/pybind11/share/cmake/pybind11/ ..
cmake --build . --config Release --target install
echo "Meshroom done"
