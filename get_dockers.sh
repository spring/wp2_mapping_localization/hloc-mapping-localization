#!/usr/bin/env bash
echo "Init docker containers"
docker pull registry.gitlab.inria.fr/spring/dockers/wp2_hloc:latest
echo "Hloc service client docker done"
docker pull colmap/colmap:latest
echo "Colmap done"
docker pull registry.gitlab.inria.fr/spring/dockers/hloc_container:latest
docker image tag registry.gitlab.inria.fr/spring/dockers/hloc_container:latest hloc:latest
docker rmi registry.gitlab.inria.fr/spring/dockers/hloc_container:latest
echo "Hloc done"