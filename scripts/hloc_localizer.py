from logging import exception
import rospy
from geometry_msgs.msg import PoseStamped
import tf
from sensor_msgs.msg import CompressedImage
import cv2
import numpy as np 
import os
import sys
import json
file_path = __file__
dir_path = os.path.dirname(file_path)
outer_dir_path = os.path.dirname(dir_path)
sys.path.append(outer_dir_path)
import src.colmap.ColmapIO as colmap
from third_party.meshroom.bin import meshroom_compute
import h5py
import torch
from hloc import matchers
from hloc import  extractors
from hloc import extract_features, match_features, pairs_from_retrieval
from hloc import localize_sfm
from pathlib import Path
from hloc.utils.read_write_model import read_images_binary
from hloc.utils.io import list_h5_names
from hloc.utils.base_model import dynamic_load

front_fisheye_image = None
rear_fisheye_image = None
CameraObjects = []

front_fisheye_topic = rospy.get_param("/hloc_loc/front_fisheye_topic")
rear_fisheye_topic = rospy.get_param("/hloc_loc/rear_fisheye_topic")
num_pairs = rospy.get_param("/hloc_loc/num_pairs")

class Hloc():
    def __init__(self, map_path):
        map_path = Path(map_path)        
        self.retrieval_conf = extract_features.confs['netvlad']
        self.feature_conf = extract_features.confs['superpoint_aachen']
        self.matcher_conf = match_features.confs['superglue']

        # database features and descriptors
        self.reference_sfm = map_path / 'sfm_superpoint+superglue'
        self.db_feature_path = map_path / 'feats-superpoint-n4096-r1024.h5'
        self.db_descriptors_path = map_path / 'global-feats-netvlad.h5'
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.db_desc = None
        self.db_data = self.get_db()
        
        self.feature_model = self.get_model(self.feature_conf)
        self.retrieval_model = self.get_model(self.retrieval_conf)
        self.matcher_model = self.get_model(self.matcher_conf, match=True)
        self.models = (self.feature_model, self.retrieval_model, self.matcher_model)

    def get_model(self, conf, match=False):
        if not match:
            Model = dynamic_load(extractors, conf['model']['name'])
        else:
            Model = dynamic_load(matchers, conf['model']['name'])

        model = Model(conf['model']).eval().to(self.device)
        return model

    def get_db(self):
        db_model = self.reference_sfm
        db_descriptors = self.db_descriptors_path

        if isinstance(db_descriptors, (Path, str)):
            db_descriptors = [db_descriptors]
        name2db = {n: i for i, p in enumerate(db_descriptors)
                   for n in list_h5_names(p)}

        images = read_images_binary(db_model / 'images.bin')
        db_names = [i.name for i in images.values()]
        if len(db_names) == 0:
            raise ValueError('Could not find any database image.')

        def get_descriptors(names, path, name2idx=None, key='global_descriptor'):
            device = 'cuda' if torch.cuda.is_available() else 'cpu'

            if name2idx is None:
                with h5py.File(str(path), 'r') as fd:
                    desc = [fd[n][key].__array__() for n in names]
            else:
                desc = []
                for n in names:
                    with h5py.File(str(path[name2idx[n]]), 'r') as fd:
                        desc.append(fd[n][key].__array__())
            return torch.from_numpy(np.stack(desc, 0)).to(device).float()
        db_desc = get_descriptors(db_names, db_descriptors, name2db)

        return (db_desc, db_names)

class Camera():
    def __init__(self, camera):
        self.camera_name = camera[0]
        self.topic = camera[1]
        self.sub = rospy.Subscriber(self.topic, CompressedImage, self.fisheye_callback, queue_size=10)
        self.image = None

    def fisheye_callback(self, msg):
        np_arr = np.frombuffer(msg.data, np.uint8)
        self.image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

def get_pose_from_image():
    global CameraObjects, hloc_obj, pose_pub
    try:
        rospy.loginfo("Writing query")
        cv2.imwrite("/root/data_folder/Query/front_fisheye_query_image.jpg", CameraObjects[0].image)
    except:
        try:
            rospy.logwarn("FRONT FISHEYE TOPIC NOT PUBLISHING. TRYING REAR FISHEYE")
            cv2.imwrite("/root/data_folder/Query/rear_fisheye_query_image.jpg", CameraObjects[1].image)
        except:
            rospy.logwarn("REAR FISHEYE NOT PUBLISHING EITHER")
            succ = False
            return succ
    pose = PoseStamped()
    st = rospy.Time.now()
    succ=True
    try:
        meshroom_compute.execute("/root/catkin_ws/src/ari_hloc/pipelines/ARI_localizer.mg", additional_params=(hloc_obj.models,hloc_obj.db_data, num_pairs) )
    except:
        succ=False
    pose.header.stamp = st
    if succ:
        loc_folder = "/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer"
        cache_folder = os.path.join(loc_folder, os.listdir("/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer")[0])
        loc_res_file = os.path.join(cache_folder, 'query_localization_results.txt')
        file_reader = open(loc_res_file, 'r')
        data_lines = file_reader.read().split("\n")
        line = data_lines[0]
        data = line.split(" ")

        _ = data[0]
        quat = data[1:5]
        quat = [float(d) for d in quat]
        w = quat[0]
        #hloc outputs in w,x,y,z format whereas ROS uses x,y,z,w   
        quat[:3] = quat[1:4]
        quat[-1] = w
        t = data[5:]
        t = np.asarray([float(tr) for tr in t])

        pose.header.frame_id = "map"
        #INVERSE FOR MAP FRAME
        pose.pose.orientation.x = -quat[0]
        pose.pose.orientation.y = -quat[1]
        pose.pose.orientation.z = -quat[2]
        pose.pose.orientation.w = quat[3]
        R_inv = tf.transformations.quaternion_matrix([pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w ])[:3,:3]
        C = -1 * R_inv @ t.reshape((3,1))
        pose.pose.position.x = C[0]
        pose.pose.position.y = C[1]
        pose.pose.position.z = C[2]

        os.system("rm  -rf /root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer")
        rospy.loginfo("Deleted Cache")

    pose_pub.publish(pose)
    return succ


if __name__=='__main__':
    C = colmap.ColmapIO()
    cameras = {
    'front_fisheye':front_fisheye_topic,
    'rear_fisheye':rear_fisheye_topic, 
    }
    
    map_outer_folder = '/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/Mapper/'
    map_folder = os.path.join(map_outer_folder, os.listdir(map_outer_folder)[0])
    
    hloc_map_outer_folder = '/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocMapCreator/'
    hloc_map_folder = os.path.join(hloc_map_outer_folder, os.listdir(hloc_map_outer_folder)[0])
    
    hloc_obj = Hloc(hloc_map_folder)

    cameras_file = os.path.join(map_folder, 'cameras.txt') 
    cameras_colmap = C.load_cameras(cameras_file)
    #CHECK BELOW
    front_fisheye_cam = cameras_colmap[1]
    if len(cameras_colmap) == 2:
        rear_fisheye_cam = cameras_colmap[2]

    os.makedirs("/root/data_folder/Query", exist_ok=True)
    line = front_fisheye_cam['model'] +  ' ' + str(front_fisheye_cam['width']) +  ' ' + str(front_fisheye_cam['height']) +  ' ' +   ' '.join(str(f) for f in front_fisheye_cam['f']) + ' ' + ' '.join(str(pp) for pp in front_fisheye_cam['pp'] ) + ' ' +  ' '.join(str(d) for d in front_fisheye_cam['rd'])  
    with open('/root/data_folder/Query/params.txt', 'w') as f:
        f.write(f"{line}\n")

    cameras_list = list(cameras.keys())
    rospy.init_node('hloc_localization')
    # s = rospy.Service('get_global_pose', hloc_pose, get_pose_from_image)
    pose_pub = rospy.Publisher('/hloc_localization/pose', PoseStamped, queue_size=10)
    r = rospy.Rate(10)

    for i in range(len(cameras_list)):
        camera = cameras_list[i]
        topic =  cameras[camera]    
        cam = Camera((camera, topic))
        CameraObjects.append(cam)
    
    while not rospy.is_shutdown():
        succ = get_pose_from_image()
        if not succ:
            rospy.logwarn("Localization failed")
        r.sleep()

    rospy.spin()
