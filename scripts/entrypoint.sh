#!/bin/bash
echo $@
source /opt/ros/noetic/setup.bash
source /root/catkin_ws/devel/setup.bash
chmod +x /root/catkin_ws/src/ari_hloc/scripts/capture_and_map.py
chmod +x /root/catkin_ws/src/ari_hloc/scripts/hloc_localizer.py
exec $@
