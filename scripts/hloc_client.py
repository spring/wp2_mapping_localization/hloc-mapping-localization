#!/usr/bin/env python
import rospy, rostopic
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from geometry_msgs.msg import PoseWithCovarianceStamped
import sys
from ari_hloc.srv import InitPose,InitPoseResponse

time_ok_min   = int(sys.argv[1])
time_ok_max   = int(sys.argv[2])
time_warn_max = int(sys.argv[3])

def initialize_pose_hloc():
    succ = False
    rospy.wait_for_service('InitPose_Hloc')
    try:
        get_pose = rospy.ServiceProxy('InitPose_Hloc', InitPose)
        resp = get_pose()
        if resp.success == True:
            succ = True
            rospy.loginfo("Sent Hloc pose to /slam/initialpose")
        else:
            rospy.logwarn("Failed to send pose")

    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)
    
    return succ 

if __name__ == '__main__':
    rospy.init_node('hloc_client')
    last_update = rospy.Time.now()
    r = rostopic.ROSTopicHz(-1)
    s = rospy.Subscriber('/slam/localization_pose', PoseWithCovarianceStamped, r.callback_hz, callback_args='/slam/localization_pose' )
    rospy.sleep(1)
    pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
    arr = DiagnosticArray()


    rate = rospy.Rate(2)
    while not rospy.is_shutdown():
        avg_rate = r.get_hz('/slam/localization_pose')
        msg = DiagnosticStatus(level=DiagnosticStatus.ERROR, name='Navigation: Localization: Hloc', message='Hloc localization failed.' )

        if avg_rate is not None:
            last_update = rospy.Time.now()

        diff = (rospy.Time.now()-last_update).secs  
        if diff >= time_ok_min and diff <= time_ok_max:
            msg = DiagnosticStatus(level=DiagnosticStatus.OK, name='Navigation: Localization: Hloc', message="Rtabmap still publishing. No need for Hloc yet")
        elif diff > time_ok_max and diff <= time_warn_max:
            msg = DiagnosticStatus(level=DiagnosticStatus.OK, name='Navigation: Localization: Hloc', message='Warning: No pose update from SLAM for "+str(diff)+" seconds. Will call Hloc soon')
        else:
            rospy.loginfo("Calling Hloc initialization service")
            success = initialize_pose_hloc()
            if not success:
                msg = DiagnosticStatus(level=DiagnosticStatus.ERROR, name='Navigation: Localization: Hloc', message='Hloc localization failed.' )
            else:
                msg = DiagnosticStatus(level=DiagnosticStatus.OK, name='Navigation: Localization: Hloc', message="Hloc pose obtained")
                last_update = rospy.Time.now()

        arr.status = [msg]
        pub.publish(arr)
        rate.sleep()

