from os import cpu_count
import numpy as np 
import sys
from pathlib import Path 
sys.path.append(str(Path(__file__).absolute().parent.parent ))
import src.colmap.ColmapIO as colmap
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation as R

C = colmap.ColmapIO()
map_folder = Path(sys.argv[1])
loc_res_file = Path(sys.argv[2])

fig = plt.figure() # figure handle to be used later
fig.clf()
ax = fig.add_subplot(111, projection='3d')

cameras, images, points3D = C.load_model(str(map_folder))
images_keys = list(images.keys())
names = np.asarray([images[key]['name'] for key in images_keys])
map_positions = []
for key in images_keys:
    img = images[key]
    name = img['name']
    camera_pos = np.asarray(img['C'])
    map_positions.append(camera_pos)
map_positions = np.asarray(map_positions)


ax.scatter(map_positions[:,0],map_positions[:,1], map_positions[:,2],'+', color='red')


file_reader = open(loc_res_file, 'r')
data_lines = file_reader.read().split("\n")
localizer_positions = []
for line in data_lines:
    data = line.split(" ")
    if len(data) <= 1:
        continue
    _ = data[0]
    quat = data[1:5]
    quat = [float(d) for d in quat]
    w = quat[0]
    quat[:3] = quat[1:4]
    quat[-1] = w
    rot = R.from_quat(np.asarray(quat))
    rot_inv = rot.inv()
    R_inv = rot_inv.as_matrix()

    t = data[5:]
    t = [float(tr) for tr in t]
    t = np.asarray(t).reshape((3,1))
    C = -1*R_inv@t
    localizer_positions.append(C)
localizer_positions = np.asarray(localizer_positions)
ax.scatter(localizer_positions[:,0],localizer_positions[:,1], localizer_positions[:,2],'+', color='blue')

plt.show()