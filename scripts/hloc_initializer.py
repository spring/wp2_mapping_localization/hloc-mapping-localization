#!/usr/bin/env python

# USE TF TO TRANSFORM POSE FROM FISHEYE TO BASE 
from Hloc import Hloc
import rospy
from geometry_msgs.msg import Pose,PoseStamped,PoseWithCovarianceStamped
from sensor_msgs.msg import CompressedImage
import cv2
import rospy
import tf
import numpy as np 
import os
import sys
file_path = __file__
dir_path = os.path.dirname(file_path)
outer_dir_path = os.path.dirname(dir_path)
sys.path.append(outer_dir_path)
import src.colmap.ColmapIO as colmap
from third_party.meshroom.bin import meshroom_compute
from ari_hloc.srv import InitPose,InitPoseResponse
import tf.transformations

front_fisheye_image = None
rear_fisheye_image = None
CameraObjects = []

localize_with = rospy.get_param("/hloc_loc/localization_camera")
front_fisheye_topic = rospy.get_param("/hloc_loc/front_fisheye_topic")
rear_fisheye_topic = rospy.get_param("/hloc_loc/rear_fisheye_topic")
num_pairs = rospy.get_param("/hloc_loc/num_pairs")

#Hacky 
rel_t = np.asarray([rospy.get_param("/hloc_loc/rel_x"), rospy.get_param("/hloc_loc/rel_y"), rospy.get_param("/hloc_loc/rel_z") ])
rel_q = np.asarray([rospy.get_param("/hloc_loc/rel_q_x"), rospy.get_param("/hloc_loc/rel_q_y"), rospy.get_param("/hloc_loc/rel_q_z"), rospy.get_param("/hloc_loc/rel_q_w")])
rel_R = tf.transformations.quaternion_matrix(rel_q)[:3,:3]
rel_transform = np.eye(4, dtype=np.float64)
rel_transform[:3,:3] = rel_R
rel_transform[:3,-1] = rel_t
hloc_movement_threshold = rospy.get_param("/hloc_loc/movement_threshold")
opt=rospy.get_param("/hloc_loc/use_calib")
localization_from = None
class Camera():
    def __init__(self, camera):
        self.camera_name = camera[0]
        self.topic = camera[1]
        self.sub = rospy.Subscriber(self.topic, CompressedImage, self.fisheye_callback, queue_size=10)
        self.image = None

    def fisheye_callback(self, msg):
        np_arr = np.frombuffer(msg.data, np.uint8)
        self.image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)


def initialize_pose(req, front_fisheye_image, rear_fisheye_image, pose_pub, hloc_object, localization_camera="front"):
    if localization_camera.lower() == 'front':
        try:
            rospy.loginfo("Writing query")
            cv2.imwrite("/root/data_folder/Query/front_fisheye_query_image.jpg", front_fisheye_image)
        except:
            try:
                rospy.logwarn("FRONT FISHEYE TOPIC NOT PUBLISHING. TRYING REAR FISHEYE")
                cv2.imwrite("/root/data_folder/Query/rear_fisheye_query_image.jpg", rear_fisheye_image)
            except:
                rospy.logwarn("REAR FISHEYE NOT PUBLISHING EITHER")
                return False
    elif localization_camera.lower() == 'rear':
        try:
            rospy.loginfo("Writing query")
            cv2.imwrite("/root/data_folder/Query/rear_fisheye_query_image.jpg", rear_fisheye_image)
        except:
            try:
                rospy.logwarn("REAR FISHEYE TOPIC NOT PUBLISHING. TRYING FRONT FISHEYE")
                cv2.imwrite("/root/data_folder/Query/rear_fisheye_query_image.jpg", rear_fisheye_image)
            except:
                rospy.logwarn("FRONT FISHEYE NOT PUBLISHING EITHER")
                return False

    hloc_pose = PoseWithCovarianceStamped()
    st = rospy.Time.now()
    succ=True
    tf_listener = tf.TransformListener()
    tf_listener.waitForTransform("base_footprint", "odom", rospy.Time(), rospy.Duration(1.0))            
    tf_listener.waitForTransform("base_footprint", "front_fisheye_camera_optical_frame", rospy.Time(), rospy.Duration(1.0))            
    (t,q) = tf_listener.lookupTransform("odom", "base_footprint", rospy.Time())
    position_before = np.asarray(t) 
    try:
        meshroom_compute.execute("/root/catkin_ws/src/ari_hloc/pipelines/ARI_localizer.mg", additional_params=(hloc_object.models,hloc_object.db_data, hloc_object.num_matching_pairs) )
    except:
        succ=False
    hloc_pose.header.stamp = st
    if succ:
        tf_listener.waitForTransform("base_footprint", "odom", rospy.Time(), rospy.Duration(0.5))            
        (t,q) = tf_listener.lookupTransform('odom', 'base_footprint', rospy.Time())
        position_after = np.asarray(t)
        if np.linalg.norm(position_after-position_before) > hloc_movement_threshold:
            rospy.logwarn("ROBOT HAS MOVED. NOT UPADING TF BASED ON HLOC")
            return True
        
        loc_folder = "/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer"
        cache_folder = os.path.join(loc_folder, os.listdir("/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer")[0])
        loc_res_file = os.path.join(cache_folder, 'query_localization_results.txt')
        file_reader = open(loc_res_file, 'r')
        data_lines = file_reader.read().split("\n")
        line = data_lines[0]
        data = line.split(" ")

        _ = data[0]
        quat_hloc = data[1:5]
        quat_hloc = [float(d) for d in quat_hloc]
        w = quat_hloc[0]
        #hloc outputs in w,x,y,z format whereas ROS uses x,y,z,w   ?
        quat = np.zeros_like(quat_hloc)
        quat[:3] = quat_hloc[1:4]
        quat[-1] = w
        t = data[5:]
        t = np.asarray([float(tr) for tr in t])

        hloc_pose.header.frame_id = "map"
        #INVERSE FOR MAP FRAME
        R_inv = tf.transformations.quaternion_matrix([-quat[0], -quat[1], -quat[2], quat[3] ])[:3,:3]
        C = -R_inv @ t.reshape((3,1))
        
        # hloc pose to base footprint pose
        (ctb,cqb) = tf_listener.lookupTransform('front_fisheye_camera_optical_frame', 'base_footprint', rospy.Time())

        if opt:
            mP_fc = np.zeros((4,4), dtype=np.float64)
            mR_fc = R_inv
            mt_fc = C
            mP_fc[:3,:3] = mR_fc
            mP_fc[:3,-1] = np.asarray(mt_fc).reshape((3,))
            mP_fc[-1,-1] = 1

            fc_Pc = np.linalg.inv(rel_transform)

            (c_t_ft,c_q_ft) = tf_listener.lookupTransform('torso_front_camera_color_optical_frame', 'front_fisheye_camera_optical_frame', rospy.Time()) 
            cP_ft = np.zeros((4,4), dtype=np.float64)
            cR_ft = tf.transformations.quaternion_matrix(c_q_ft)[:3,:3]
            ct_ft = c_t_ft
            cP_ft[:3,:3] = cR_ft
            cP_ft[:3,-1] = np.asarray(ct_ft).reshape((3,))
            cP_ft[-1,-1] = 1

            ft_t_b = ctb
            ft_q_b = cqb

            ft_Pb = np.zeros((4,4), dtype=np.float64)
            ft_Rb = tf.transformations.quaternion_matrix(ft_q_b)[:3,:3]
            ft_Pb[:3,:3] = ft_Rb
            ft_Pb[:3,-1] = ft_t_b
            ft_Pb[-1,-1] = 1

            mPb = mP_fc@fc_Pc@cP_ft@ft_Pb
            mRb = np.zeros_like(ft_Pb)
            mRb[:3,:3] = mPb[:3,:3]
            mRb[-1,-1] = 1
            mtb = mPb[:3,-1]
            mqb = tf.transformations.quaternion_from_matrix(mRb)       

            hloc_pose.pose.pose.position.x = mtb[0]
            hloc_pose.pose.pose.position.y = mtb[1]
            hloc_pose.pose.pose.position.z = mtb[2]
    
            hloc_pose.pose.pose.orientation.x = mqb[0]
            hloc_pose.pose.pose.orientation.y = mqb[1]
            hloc_pose.pose.pose.orientation.z = mqb[2]
            hloc_pose.pose.pose.orientation.w = mqb[3]

        else:
            wPc = np.zeros((4,4), dtype=np.float64)
            wRc = R_inv
            wtc = C
            wPc[:3,:3] = wRc
            wPc[:3,-1] = wtc.reshape((3,))
            wPc[-1,-1] = 1
    
            cPb = np.zeros((4,4), dtype=np.float64)
            cRb = tf.transformations.quaternion_matrix(cqb)[:3,:3]
            cPb[:3,:3] = cRb
            cPb[:3,-1] = ctb
            cPb[-1,-1] = 1
    
            wPb_prime = wPc@cPb
            wPb = rel_transform@wPb_prime
            wRb = np.zeros_like(cPb)
            wRb[:3,:3] = wPb[:3,:3]
            wRb[-1,-1] = 1
            wtb = wPb[:3,-1]
            wqb = tf.transformations.quaternion_from_matrix(wRb)       
    
            hloc_pose.pose.pose.position.x = wtb[0]
            hloc_pose.pose.pose.position.y = wtb[1]
            hloc_pose.pose.pose.position.z = wtb[2]
    
            hloc_pose.pose.pose.orientation.x = wqb[0]
            hloc_pose.pose.pose.orientation.y = wqb[1]
            hloc_pose.pose.pose.orientation.z = wqb[2]
            hloc_pose.pose.pose.orientation.w = wqb[3]
        
        os.system("rm  -rf /root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocLocalizer")
        rospy.loginfo("Deleted Cache")
        pose_pub.publish(hloc_pose)
        res = InitPoseResponse(succ)
    return res

if __name__=='__main__':
    C = colmap.ColmapIO()
    cameras = {
    'front_fisheye':front_fisheye_topic,
    'rear_fisheye':rear_fisheye_topic, 
    }

    map_outer_folder = '/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/Mapper/'
    map_folder = os.path.join(map_outer_folder, os.listdir(map_outer_folder)[0])

    hloc_map_outer_folder = '/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/HlocMapCreator/'
    hloc_map_folder = os.path.join(hloc_map_outer_folder, os.listdir(hloc_map_outer_folder)[0])
    hloc_obj = Hloc(hloc_map_folder, num_pairs=num_pairs)

    cameras_file = os.path.join(map_folder, 'cameras.txt') 
    cameras_colmap = C.load_cameras(cameras_file)
    front_fisheye_cam = cameras_colmap[1]
    if len(cameras_colmap) == 2:
        rear_fisheye_cam = cameras_colmap[2]
    os.makedirs("/root/data_folder/Query", exist_ok=True)
    if localize_with.lower() == 'front':
        line = front_fisheye_cam['model'] +  ' ' + str(front_fisheye_cam['width']) +  ' ' + str(front_fisheye_cam['height']) +  ' ' +   ' '.join(str(f) for f in front_fisheye_cam['f']) + ' ' + ' '.join(str(pp) for pp in front_fisheye_cam['pp'] ) + ' ' +  ' '.join(str(d) for d in front_fisheye_cam['rd'])  
    elif localize_with.lower() == 'rear':
        line = rear_fisheye_cam['model'] +  ' ' + str(rear_fisheye_cam['width']) +  ' ' + str(rear_fisheye_cam['height']) +  ' ' +   ' '.join(str(f) for f in rear_fisheye_cam['f']) + ' ' + ' '.join(str(pp) for pp in rear_fisheye_cam['pp'] ) + ' ' +  ' '.join(str(d) for d in rear_fisheye_cam['rd'])  

    with open('/root/data_folder/Query/params.txt', 'w') as f:
        f.write(f"{line}\n")
    cameras_list = list(cameras.keys())

    pose_init_topic = "/slam/initialpose"
    rospy.init_node('hloc_InitPose')
    pose_pub = rospy.Publisher(pose_init_topic, PoseWithCovarianceStamped, queue_size=10)
        
    CameraObjects = [Camera((cameras_list[i], cameras[cameras_list[i]]))  for i in range(len(cameras_list))]

    pose_callback_lambda = lambda x: initialize_pose(x,CameraObjects[0].image, CameraObjects[1].image, pose_pub, hloc_obj, localization_camera=localize_with)
    s = rospy.Service('InitPose_Hloc',InitPose, pose_callback_lambda)

    rospy.spin()