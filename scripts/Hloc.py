from pathlib import Path
from hloc import matchers
from hloc import  extractors
from hloc import extract_features, match_features, pairs_from_retrieval
from hloc import localize_sfm
from hloc.utils.read_write_model import read_images_binary
from hloc.utils.io import list_h5_names
from hloc.utils.base_model import dynamic_load
import h5py
import torch
import numpy as np


class Hloc():
    def __init__(self, map_path, num_pairs=25):
        map_path = Path(map_path)        
        self.retrieval_conf = extract_features.confs['netvlad']
        self.feature_conf = extract_features.confs['superpoint_aachen']
        self.matcher_conf = match_features.confs['superglue']

        # database features and descriptors
        self.reference_sfm = map_path / 'sfm_superpoint+superglue'
        self.db_feature_path = map_path / 'feats-superpoint-n4096-r1024.h5'
        self.db_descriptors_path = map_path / 'global-feats-netvlad.h5'
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.db_desc = None
        self.db_data = self.get_db()
        
        self.feature_model = self.get_model(self.feature_conf)
        self.retrieval_model = self.get_model(self.retrieval_conf)
        self.matcher_model = self.get_model(self.matcher_conf, match=True)
        self.models = (self.feature_model, self.retrieval_model, self.matcher_model)
        self.num_matching_pairs = num_pairs

    def get_model(self, conf, match=False):
        if not match:
            Model = dynamic_load(extractors, conf['model']['name'])
        else:
            Model = dynamic_load(matchers, conf['model']['name'])

        model = Model(conf['model']).eval().to(self.device)
        return model

    def get_db(self):
        db_model = self.reference_sfm
        db_descriptors = self.db_descriptors_path

        if isinstance(db_descriptors, (Path, str)):
            db_descriptors = [db_descriptors]
        name2db = {n: i for i, p in enumerate(db_descriptors)
                   for n in list_h5_names(p)}

        images = read_images_binary(db_model / 'images.bin')
        db_names = [i.name for i in images.values()]
        if len(db_names) == 0:
            raise ValueError('Could not find any database image.')

        def get_descriptors(names, path, name2idx=None, key='global_descriptor'):
            device = 'cuda' if torch.cuda.is_available() else 'cpu'

            if name2idx is None:
                with h5py.File(str(path), 'r') as fd:
                    desc = [fd[n][key].__array__() for n in names]
            else:
                desc = []
                for n in names:
                    with h5py.File(str(path[name2idx[n]]), 'r') as fd:
                        desc.append(fd[n][key].__array__())
            return torch.from_numpy(np.stack(desc, 0)).to(device).float()
        db_desc = get_descriptors(db_names, db_descriptors, name2db)

        return (db_desc, db_names)
