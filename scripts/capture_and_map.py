#!/usr/bin/env python

# USE POSE OF FISHEYE WITH TF, RATHER THAN BASE

from ari_hloc.srv import map_capture, map_captureResponse
from logging import exception
import rospy
import numpy as np
from pathlib import Path
import cv2
import sys
import os
file_path = __file__
dir_path = os.path.dirname(file_path)
outer_dir_path = os.path.dirname(dir_path)
sys.path.append(outer_dir_path)
from third_party.meshroom.bin import meshroom_compute
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseWithCovarianceStamped, Point, Quaternion 
from cv_bridge import CvBridge
import subprocess as sp 
import json
import tf 
import os
# import argparse
import shutil

position_diff_thresh = 0.10 # m 
orientation_diff_thresh = 10 # degree

save_folder = rospy.get_param("/capture_and_map/save_folder")
host_cache_folder = rospy.get_param("/capture_and_map/cache_folder")
pose_topic = rospy.get_param("/capture_and_map/pose_topic")
front_fisheye_topic = rospy.get_param("/capture_and_map/front_fisheye_topic")
rear_fisheye_topic = rospy.get_param("/capture_and_map/rear_fisheye_topic")

pose_file = str(save_folder)+'/robot_pose.json'
pose_list = []

robot_position = None
robot_orientation = None

opt = rospy.get_param("/capture_and_map/use_calib")

if opt:
    rel_t = np.asarray([rospy.get_param("/capture_and_map/rel_x"), rospy.get_param("/capture_and_map/rel_y"), rospy.get_param("/capture_and_map/rel_z") ])
    rel_q = np.asarray([rospy.get_param("/capture_and_map/rel_q_x"), rospy.get_param("/capture_and_map/rel_q_y"), rospy.get_param("/capture_and_map/rel_q_z"), rospy.get_param("/capture_and_map/rel_q_w")])
    rel_R = tf.transformations.quaternion_matrix(rel_q)[:3,:3]
    rel_transform = np.eye(4, dtype=np.float64)
    rel_transform[:3,:3] = rel_R
    rel_transform[:3,-1] = rel_t

def on_shut(req):
    rospy.loginfo("FINISHED IMAGE CAPTURE FOR MAPPING. SAVING POSES AND RUNNING MAPPER NOW")
    with open(pose_file, 'w') as fout:
        json.dump(pose_list, fout)
    rospy.loginfo("saved poses. Creating imagefolders file now")
    camera_names = cameras.keys()
    with open(os.path.join(save_folder, 'ImageFolders'), 'w') as f:
        for line in camera_names:
            f.write(f"{line}\n")
    rospy.loginfo("Deleting previous cache")
    cache_folder = "/root/catkin_ws/src/ari_hloc/pipelines/MeshroomCache/"
    files = os.listdir(cache_folder)
    for file in files:
        file_path = os.path.join(cache_folder, file)
        shutil.rmtree(file_path, ignore_errors=True)
    succ=True
    
    # COPY VOCAB TREE FOR MATCHING TO CACHE SO THAT IT CAN BE MOUNTED 
    shutil.copyfile("/root/catkin_ws/src/ari_hloc/vocab_tree_flickr100K_words32K.bin", cache_folder+"/vocab_tree_flickr100K_words32K.bin")

    try:
        meshroom_compute.execute("/root/catkin_ws/src/ari_hloc/pipelines/mapper.mg", additional_params=host_cache_folder )
    except:
        succ=False

    res = map_captureResponse(succ)
    return res

def pose_callback(msg):
    global robot_position, robot_orientation, pose_list
    pose_dict = {}

    # tf_listener.waitForTransform("map", "front_fisheye_camera_optical_frame", rospy.Time(), rospy.Duration(1.0))            
    tf_listener.waitForTransform("map", "front_fisheye_camera_optical_frame", rospy.Time(), rospy.Duration(1.0))            
    (C,q_inv) = tf_listener.lookupTransform('map', 'front_fisheye_camera_optical_frame', rospy.Time()) # Gets q_inv,C (in the map frame)

    time = rospy.Time.now()
    if opt:
        (ft_t_c,ft_q_c) = tf_listener.lookupTransform('front_fisheye_camera_optical_frame', 'torso_front_camera_color_optical_frame', rospy.Time()) 
        mP_ft = np.zeros((4,4), dtype=np.float64)
        mR_ft = tf.transformations.quaternion_matrix(q_inv)[:3,:3]
        mt_ft = C
        mP_ft[:3,:3] = mR_ft
        mP_ft[:3,-1] = np.asarray(mt_ft).reshape((3,))
        mP_ft[-1,-1] = 1
    
        ft_Pc = np.zeros((4,4), dtype=np.float64)
        ft_Rc = tf.transformations.quaternion_matrix(ft_q_c)[:3,:3]
        ft_tc = ft_t_c
        ft_Pc[:3,:3] = ft_Rc
        ft_Pc[:3,-1] = np.asarray(ft_tc).reshape((3,))
        ft_Pc[-1,-1] = 1
    
        cP_fc = rel_transform
    
        mP_fc = mP_ft@ft_Pc@cP_fc
        mR_fc = np.zeros_like(cP_fc)
        mR_fc[:3,:3] = mP_fc[:3,:3]
        mR_fc[-1,-1] = 1
    
        robot_position = mP_fc[:3,-1]
        robot_orientation = tf.transformations.quaternion_from_matrix(mR_fc)
        pose_dict['position'] = list(robot_position.ravel())
        pose_dict['orientation'] = list(robot_orientation.ravel())
    else:
        robot_position = C
        robot_orientation = q_inv
        pose_dict['position'] = C
        pose_dict['orientation'] = q_inv
    pose_dict['time'] = str(time)
    pose_list.append(pose_dict)

class Camera():
    def __init__(self, camera):
        self.last_position = [0,0,0]
        self.last_orientation = [0,0,0,1]

        self.br = CvBridge()
        self.camera_name = camera[0]
        self.inner_folder =  str(save_folder)+'/'+self.camera_name
        self.inner_folder_path = Path(self.inner_folder)
        try:
            self.inner_folder_path.mkdir(parents=True,exist_ok=True)
        except:
            self.inner_folder_path.mkdir(parents=True)

        self.topic = camera[1]
        self.compressed = 1

        if self.compressed==1:
            self.sub = rospy.Subscriber(self.topic, CompressedImage, self.compressed_callback, queue_size=10)
        else:
            self.sub = rospy.Subscriber(self.topic, Image, self.image_callback, queue_size=10)                
        
    def compressed_callback(self,msg):
        global robot_position, robot_orientation
        time = rospy.Time.now()
        if robot_position is None or robot_orientation is None:
            try:
                np_arr = np.frombuffer(msg.data, np.uint8)
                image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                rospy.loginfo("WRITING IMAGE")
                cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
            except:
                pass
        else:

            position_diff = np.linalg.norm(np.asarray(self.last_position) - np.asarray(robot_position))
            q1_inv = [0]*4
            q2 = [0]*4 
            q1_inv[0] = self.last_orientation[0]
            q1_inv[1] = self.last_orientation[1]
            q1_inv[2] = self.last_orientation[2]
            q1_inv[3] = -self.last_orientation[3] # Negate for inverse
        
            q2[0] = robot_orientation[0]
            q2[1] = robot_orientation[1]
            q2[2] = robot_orientation[2]
            q2[3] = robot_orientation[3]
            
            relative_orientation = tf.transformations.quaternion_multiply(q2, q1_inv)
            relative_orientation_euler_deg = np.rad2deg(tf.transformations.euler_from_quaternion(relative_orientation))
            if position_diff > position_diff_thresh or np.linalg.norm(relative_orientation_euler_deg) > orientation_diff_thresh or position_diff==0 or np.linalg.norm(relative_orientation_euler_deg)==0:
                try:
                    np_arr = np.frombuffer(msg.data, np.uint8)
                    image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                    cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
                except:
                    pass

                self.last_position = robot_position
                self.last_orientation = robot_orientation                

    def image_callback(self,msg):
        global robot_position, robot_orientation
        # time = msg.header.stamp
        time = rospy.Time.now()
        if robot_position is None or robot_orientation is None:
            try:
                image = self.br.imgmsg_to_cv2(msg)
                cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
            except:
                pass
        else: 
            position_diff = np.linalg.norm(self.last_position - robot_position)
            q1_inv = []
            q2 = [] 
            q1_inv[0] = self.last_orientation.x
            q1_inv[1] = self.last_orientation.y
            q1_inv[2] = self.last_orientation.z
            q1_inv[3] = -self.last_orientation.w # Negate for inverse
        
            q2[0] = robot_orientation.x
            q2[1] = robot_orientation.y
            q2[2] = robot_orientation.z
            q2[3] = robot_orientation.w
            
            relative_orientation = tf.transformations.quaternion_multiply(q2, q1_inv)
            relative_orientation_euler_deg = np.rad2deg(tf.transformations.euler_from_quaternion(relative_orientation))
            
            if position_diff > position_diff_thresh or relative_orientation_euler_deg > orientation_diff_thresh:
                try:
                    image = self.br.imgmsg_to_cv2(msg)
                    cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
                except:
                    pass



if __name__=='__main__':
    cameras = {
    'front_fisheye':front_fisheye_topic,
    'rear_fisheye':rear_fisheye_topic, 
    }
    cameras_list = list(cameras.keys())
    rospy.init_node('image_extractor')
    tf_listener = tf.TransformListener()
    tf_listener.waitForTransform("front_fisheye_camera_optical_frame", "map", rospy.Time(), rospy.Duration(5.0))
    
    pose_sub = rospy.Subscriber(pose_topic, PoseWithCovarianceStamped, pose_callback, queue_size=10)
    s = rospy.Service('stop_capture',map_capture , on_shut)
    
    for i in range(len(cameras_list)):
        camera = cameras_list[i]
        topic =  cameras[camera]    
        cam = Camera((camera, topic))

    rospy.spin()