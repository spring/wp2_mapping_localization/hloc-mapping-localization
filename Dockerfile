# syntax=docker/dockerfile:1
FROM nvidia/cuda:11.7.1-runtime-ubuntu20.04 AS builder
ARG PYTHON_VERSION=3.8
SHELL ["/bin/bash", "-c"]
# ARG SSH_KEY=docker_id_rsa
# ADD ${SSH_KEY} /root/.ssh/id_rsa
# ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
# ARG CACHEBUST=3

WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime

RUN apt-get update && apt-get install -y \
    wget \
    libsm6\
    libglu1-mesa\
    libxi-dev\
    libxmu-dev\
    libglu1-mesa-dev\
    libxrender-dev\
    git \
    cmake \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    mlocate \
    docker.io \
    pkg-config && \
    wget -O- http://neuro.debian.net/lists/xenial.us-ca.full |  tee /etc/apt/sources.list.d/neurodebian.sources.list && \
    apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xA5D32F012649A5A9 && \
    apt-get install -y unzip wget software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get -y update && \
    apt-get install -y python${PYTHON_VERSION}

RUN apt-get install python3-distutils -y && wget https://bootstrap.pypa.io/get-pip.py && python${PYTHON_VERSION} get-pip.py && \
    mkdir -p ~/catkin_ws/src
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python${PYTHON_VERSION} 1

RUN wget --no-check-certificate https://cvg-data.inf.ethz.ch/hloc/netvlad/Pitts30K_struct.mat -O /root/VGG16-NetVLAD-Pitts30K.mat
COPY ./requirements.txt /root/requirements.txt
RUN pip3 install --upgrade pip && pip3 install -r /root/requirements.txt 
#INSTALL ROS
# RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -

RUN echo "Installing ROS Noetic..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ros-noetic-desktop && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

COPY . /root/catkin_ws/src/ari_hloc/
WORKDIR /root/catkin_ws/src/ari_hloc/
RUN mkdir -p ./third_party/Hierarchical-Localization/third_party/netvlad && mv /root/VGG16-NetVLAD-Pitts30K.mat ./third_party/Hierarchical-Localization/third_party/netvlad/
RUN ln -s /usr/bin/python3 /usr/bin/python && bash init.sh 
WORKDIR /root/catkin_ws/src/ari_hloc/third_party/Hierarchical-Localization
RUN python -m pip install --user -e .

# BUILD
WORKDIR /root/catkin_ws
RUN pip3 uninstall -y enum34 && source /opt/ros/noetic/setup.bash && catkin build 

RUN chmod +x /root/catkin_ws/src/ari_hloc/scripts/entrypoint.sh
ENTRYPOINT ["/root/catkin_ws/src/ari_hloc/scripts/entrypoint.sh","roslaunch","ari_hloc","hloc_map_loc.launch"] 
CMD ["hloc_localization_mode:=True", "pose_topic:=/robot_pose"]