#!/usr/bin/env python
import argparse
import sys
import os

dir_path = __file__
dir_path = os.path.dirname(dir_path)
dir_path = os.path.dirname(dir_path)
sys.path.append(dir_path)
import meshroom
meshroom.setupEnvironment()

import meshroom.core.graph
from meshroom.core.node import Status

def execute(graphFile, cache=None, node=None, extern=False, forceStatus=False, forceCompute=False, iteration=-1, toNode=None, additional_params=None):
    graph = meshroom.core.graph.loadGraph(graphFile)
    if cache:
        graph.cacheDir = cache
    graph.update()

    if node:
        # Execute the node
        node = graph.findNode(node)
        submittedStatuses = [Status.RUNNING]
        if not extern:
            # If running as "extern", the task is supposed to have the status SUBMITTED.
            # If not running as "extern", the SUBMITTED status should generate a warning.
            submittedStatuses.append(Status.SUBMITTED)
        if not forceStatus and not forceCompute:
            if iteration != -1:
                chunks = [node.chunks[iteration]]
            else:
                chunks = node.chunks
            for chunk in chunks:
                if chunk.status.status in submittedStatuses:
                    print('Warning: Node is already submitted with status "{}". See file: "{}"'.format(chunk.status.status.name, chunk.statusFile))
                    # sys.exit(-1)
        if iteration != -1:
            chunk = node.chunks[iteration]
            chunk.process(forceCompute)
        else:
            node.process(forceCompute)
    else:
        if iteration != -1:
            print('Error: "--iteration" only make sense when used with "--node".')
            sys.exit(-1)
        toNodes = None
        if toNode:
            toNodes = graph.findNodes([toNode])

        meshroom.core.graph.executeGraph(graph, toNodes=toNodes, forceCompute=forceCompute, forceStatus=forceStatus, additional_params=additional_params)
