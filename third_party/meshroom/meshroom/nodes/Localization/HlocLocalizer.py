from __future__ import print_function
from genericpath import isfile
# from tkinter import image_names

from src.holo.HoloIO import HoloIO

__version__ = '0.1'

from meshroom.core import desc
import shutil
import os
import sys
import numpy as np
from pathlib import Path
from shutil import copy2

# import mapper packages
dir_path = __file__
for i in range(6):
    dir_path = os.path.dirname(dir_path)
sys.path.append(dir_path)
from src.utils.UtilsContainers import UtilsContainers
from src.utils.UtilsMath import UtilsMath
from src.colmap.ColmapIO import ColmapIO
from src.holo.HoloIO import HoloIO
from src.hloc.Hloc import Hloc
from hloc import extract_features, match_features, pairs_from_retrieval
from hloc import localize_sfm

def run_hloc(map_folder, query_file, output_folder, models, db_data, num_pairs = 50):
    feature_model = models[0]
    retrieval_model = models[1]
    matcher_model = models[2]
    db_desc=db_data[0] 
    db_names=db_data[1]
    
    map_path = Path(map_folder) 
    db_images_path = map_path
    query_folder_path = Path(query_file).parent
    query_images_path = query_folder_path
    # setting 
    retrieval_conf = extract_features.confs['netvlad']
    feature_conf = extract_features.confs['superpoint_aachen']
    matcher_conf = match_features.confs['superglue']

    # database features and descriptors
    reference_sfm = map_path / 'sfm_superpoint+superglue'
    db_feature_path = map_path / 'feats-superpoint-n4096-r1024.h5'

    # output paths
    output_path = Path(output_folder)
    query_pairs_path = output_path / 'pairs-query-netvlad50.txt' 
    results_path = output_path / 'query_localization_results.txt' 


    # ---------------------------------------------------
    #              localization process 
    # ---------------------------------------------------

    # detect feature points on query images, extract NetVLAD descriptors for query images
    query_feature_path = extract_features.main(feature_conf, feature_model, query_images_path, output_path)
    query_descriptors_path = extract_features.main(retrieval_conf, retrieval_model, query_images_path, output_path)
    # global descriptor matching
    pairs_from_retrieval.main(query_descriptors_path, query_pairs_path, num_pairs, query_prefix='query', \
        db_names=db_names, db_desc = db_desc)
    pairs_from_retrieval.main(query_descriptors_path, query_pairs_path, num_pairs, query_prefix='query', \
        db_names=db_names, db_desc = db_desc )

    # feature points matching
    loc_match_path = match_features.main(matcher_conf, matcher_model, query_pairs_path, feature_conf['output'],\
        output_path, None, db_feature_path)

    # calculate the poses in global coordinate system
    localize_sfm.main(
        reference_sfm,
        Path(query_file),
        query_pairs_path,
        query_feature_path,
        loc_match_path,
        results_path,
        covisibility_clustering=False)  # not required with SuperPoint+SuperGlue


class HlocLocalizer(desc.Node):

    category = 'Localization'
    documentation = '''
Runs Hloc localization on input images.
'''

    inputs = [
        desc.File(
            name='hlocMapDir',
            label='Outer Hloc Map directory',
            description='Outer directory which contains the Hloc map directory (database, images, features)',
            value='/root/data/catkin_ws/src/hloc_mapper/pipelines/MeshroomCache/HlocMapCreator',
            uid=[0],
        ),
        desc.File(
            name='localSfM',
            label='Local SfM',
            description='Path to folder with images and local SfM.',
            value='',
            uid=[0],
        ),
        desc.BoolParam(
            name='imagesRig',
            label='Use rig of images',
            description='''
            Compute generalized absolute pose for the rig of cameras.
            Require at the input camera poses of the query images.''',
            value='True',
            uid=[],
        ),
        desc.BoolParam(
            name='copyDensePts', 
            label='Copy dense points',
            description='''Copy dense point cloud if available in map folder.''',
            value=False, 
            uid=[0]
        ),
        desc.ChoiceParam(
            name='verboseLevel',
            label='Verbose Level',
            description='''verbosity level (critical, error, warning, info, debug).''',
            value='info',
            values=['critical', 'error', 'warning', 'info', 'debug'],
            exclusive=True,
            uid=[],
            ),
        ]

    outputs = [
        desc.File(
            name='output',
            label='Output folder',
            description='',
            value=desc.Node.internalFolder,
            uid=[],
        ),
        desc.File(
            name='image_pairs',
            label='Image Pairs',
            description='',
            value=os.path.join(desc.Node.internalFolder,'image_pairs.txt'),
            uid=[],
        ),
        desc.File(
            name='localization',
            label='Localization results',
            description='',
            value=os.path.join(desc.Node.internalFolder,'query_localization_results.txt'),
            uid=[],
        ),
        desc.File(
            name='densePts',
            label='Dense point cloud',
            description='',
            value=os.path.join(desc.Node.internalFolder,'model.obj'),
            uid=[],
        )
    ]

    def copy_map_images(self, images, map_folder, output_folder):
        for img in images.values():
            new_img_path = os.path.join(output_folder, img['name'])
            Path(new_img_path).parent.mkdir(parents=True, exist_ok=True)
            shutil.copy2(os.path.join(map_folder,img['name']), new_img_path)

    def get_image_pairs_from_viewgraph(self, images, view_graph, min_common_pts = 50):
        order_to_image_id = {}  
        for i, image_id in enumerate(images): 
            order_to_image_id[i] = image_id

        list_image_pairs = []
        img_pair_ids = np.where(view_graph>min_common_pts)
        for i in range(np.shape(img_pair_ids)[1]):
            image1_id = order_to_image_id[img_pair_ids[0][i]]
            image2_id = order_to_image_id[img_pair_ids[1][i]]
            list_image_pairs.append(f"{images[image1_id]['name']} {images[image2_id]['name']}\n") 
        
        return list_image_pairs


    def processChunk(self, chunk, additional_params=None):
        try:
            chunk.logManager.start(chunk.node.verboseLevel.value)
            if not chunk.node.hlocMapDir.value:
                chunk.logger.warning('Nothing to process, Hloc map required')
                return

            

            # setup paths
            output_folder = chunk.node.output.value
            
            #Copy Query images
            loc_query_folder = os.path.join(output_folder, 'query')
            if not os.path.isdir(loc_query_folder):
                os.mkdir(loc_query_folder)

            og_query_folder = "/root/data_folder/Query"
            query_img_ct = 0 
            for query_file_name in os.listdir(og_query_folder):
                query_file_path = os.path.join(og_query_folder, query_file_name) #assuming query folder has only one image
                if "jpg" in query_file_name or "png" in query_file_name:
                    query_image_name = query_file_name
                    query_image_path = query_file_path
                    shutil.move(query_image_path, os.path.join(loc_query_folder, query_image_name))
                    query_img_ct += 1
                else:
                    shutil.copyfile(query_file_path, os.path.join(loc_query_folder, query_file_name))

            query_file = os.path.join(chunk.node.output.value, "query_file.txt")
            params_file = open(os.path.join(loc_query_folder, "params.txt"), 'r')
            params_line = params_file.readline()
            
            # Write into query file
            lines = []
            ct=0
            with open(query_file, 'w') as f:
                for query_file_name in os.listdir(loc_query_folder):
                    if "jpg" in query_file_name or "png" in query_file_name:
                        query_image_name = query_file_name
                        queryPath = "query/"+query_image_name+' '
                        line = queryPath +  params_line
                        f.write(f"{line}\n")


            cache_dir = os.path.dirname(os.path.dirname(output_folder))
            if len(os.listdir(chunk.node.hlocMapDir.value)) < 1:
                chunk.logger.error("NO HLOC MAP")
                return
            elif len(os.listdir(chunk.node.hlocMapDir.value)) > 1:
                chunk.logger.warning("MORE THAN 1 MAP. CHOOSING FIRST ONE")
            map_folder = os.path.join(chunk.node.hlocMapDir.value, os.listdir(chunk.node.hlocMapDir.value)[0])
            relative_output_folder = output_folder.replace(cache_dir, '/data')
            relative_map_folder = map_folder.replace(cache_dir, '/data')
            relative_query_file = query_file.replace(cache_dir, '/data')
            relative_corresp_path = os.path.join(relative_output_folder,'corresp_2d-3d.npy')
            relative_query_poses_path = relative_query_file.replace('hloc_queries.txt', 'hloc_queries_poses.txt')

            chunk.logger.info('Init containers ...')



            chunk.logger.info('Running localization ...')   
            run_hloc(map_folder, query_file, output_folder, additional_params[0], additional_params[1], num_pairs=additional_params[2])
            # hloc_container.command('sh /app/eval.sh ' + relative_map_folder + ' ' + relative_query_file + ' ' + relative_output_folder)
            

                
            if chunk.node.imagesRig.value:
                poselib_container = UtilsContainers('singularity', dir_path + '/poselib.sif', cache_dir)
                chunk.logger.info('Running generalized absolute pose ...')   
                poselib_container.command('sh /app/eval.sh ' + relative_corresp_path + ' ' + relative_query_poses_path)

                chunk.logger.info('Copy employed images from map to working directory ...')  
                colmap_io = ColmapIO()
                db_cameras, db_images, db_points3D = colmap_io.load_model(output_folder)
                self.copy_map_images(db_images, map_folder, output_folder)

                chunk.logger.info('Copy employed images from local sfm to working directory ...')  
                um = UtilsMath()
                if chunk.node.localSfM.value:
                    holo_io = HoloIO()
                    holo_io.copy_all_images(chunk.node.localSfM.value, output_folder)
                    
                    chunk.logger.info('Updating the local sfm ...') 
                    hloc = Hloc()
                    q_cameras, q_images, q_points3D = colmap_io.load_model(chunk.node.localSfM.value)
                    # loc_images = hloc.get_imgs_from_localization_results(chunk.node.localization.value)   # localization of individual images
                    gap_file_path = os.path.join(output_folder,'generalized_absolute_pose.txt')
                    transform = hloc.read_generalized_absolute_pose_results(gap_file_path)
                    cameras, images, points3D = um.align_local_and_global_sfm(db_cameras, db_images, db_points3D, \
                        q_cameras, q_images, q_points3D, transform)
                    colmap_io.write_model(output_folder, cameras, images, points3D)

                chunk.logger.info('Extract and write down image pairs to match ...') 
                _, db_view_graph = um.get_view_graph(db_images, db_points3D)
                db_pairs = self.get_image_pairs_from_viewgraph(db_images, db_view_graph)
                _, q_view_graph = um.get_view_graph(q_images, q_points3D)
                q_pairs = self.get_image_pairs_from_viewgraph(q_images, q_view_graph)
                with open(chunk.node.image_pairs.value, 'a') as image_pairs_file:
                    image_pairs_file.write(''.join(db_pairs))
                    image_pairs_file.write(''.join(q_pairs))

            # copy dense point cloud if available
            if chunk.node.copyDensePts.value and os.path.isfile(chunk.node.hlocMapDir.value + '/model.obj'):
                copy2(chunk.node.hlocMapDir.value + '/model.obj' , output_folder)

            chunk.logger.info('Localization done.') 

        except AssertionError as err:
            chunk.logger.error('Error in hlocLocalizer selector: ' + err)
        finally:
            chunk.logManager.end()
