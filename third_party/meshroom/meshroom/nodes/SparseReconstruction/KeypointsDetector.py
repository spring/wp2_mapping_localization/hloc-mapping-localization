from __future__ import print_function
import shutil

__version__ = '0.1'

from meshroom.core import desc
import os
import sys
from pathlib import Path
from shutil import copy2

# import mapper packages
dir_path = __file__
for i in range(6):
    dir_path = os.path.dirname(dir_path)
sys.path.append(dir_path)
from src.holo.HoloIO import HoloIO
from src.colmap.Colmap import Colmap
from src.utils.UtilsContainers import UtilsContainers


class KeypointsDetector(desc.Node):

    category = 'Sparse Reconstruction'
    documentation = '''
This node compute keypoints in the images and save them into the database.
The database is in the COLMAP format.
'''

    inputs = [
        desc.File(
            name='imagesFolder',
            label='Directory with images',
            description='''
            The directory containing input images.
            The subdirectories are specified in images.txt.''',
            value='',
            uid=[0],
        ),
        desc.File(
            name='database',
            label='Input database',
            description='''
            The database with keypoints and matches for some subreconstruction. 
            The keypoints will be find for the new images.''',
            value='',
            uid=[0],
        ),
        desc.ChoiceParam(
            name='algorithm',
            label='Detector',
            description='The algorithm used to extract keypoints and descriptors.',
            value='SIFT',
            values=['SIFT'],
            uid=[0],
            exclusive=True,
        ),  
        desc.ChoiceParam(
            name='cameraModel',
            label='Camera Model',
            description='COLMAP Camera Model of the camera you are using.',
            value='OPENCV_FISHEYE',
            values=['RADIAL', 'PINHOLE', 'SIMPLE_PINHOLE', 'OPENCV_FISHEYE'],
            uid=[0],
            exclusive=True,
        ),        
        desc.BoolParam(
            name='removeImages', 
            label='Remove images',
            description='Remove images from cache after keypoint detection.',
            value=True,
            uid=[],
        ),  
        desc.ChoiceParam(
            name='verboseLevel',
            label='Verbose Level',
            description='''verbosity level (critical, error, warning, info, debug).''',
            value='info',
            values=['critical', 'error', 'warning', 'info', 'debug'],
            exclusive=True,
            uid=[],
            ),
        ]

    outputs = [
        desc.File(
            name='output',
            label='Database file',
            description='',
            value=os.path.join(desc.Node.internalFolder,'database.db'),
            uid=[],
            ),
    ]

    def processChunk(self, chunk, additional_params=None):
        try:
            chunk.logManager.start(chunk.node.verboseLevel.value)
            
            host_cache_folder = None
            if additional_params is not None:
                host_cache_folder = additional_params
                chunk.logger.info(f"HOST CACHE FOLDER:  {host_cache_folder}")
            
            if not chunk.node.imagesFolder:
                chunk.logger.warning('Folder with images is missing.')
                return
            if not chunk.node.output.value:
                return

            chunk.logger.info('Start matching.')
            out_dir = os.path.dirname(chunk.node.output.value)
            holo_io = HoloIO()

            # copy images/image_pairs.txt to working directory
            holo_io.copy_all_images(chunk.node.imagesFolder.value, out_dir)
            
            # init contriners
            chunk.logger.info('Init COLMAP container')
            if sys.platform == 'win32':
                out_dir = out_dir[0].lower() + out_dir[1::]
                colmap_container = UtilsContainers('docker', 'colmap/colmap', '/host_mnt/' + out_dir.replace(':',''))
            else:
                output_folder_path = Path(out_dir)
                cache_folder = str(output_folder_path.parent.parent)
                if host_cache_folder is not None:
                    if host_cache_folder[-1] == '/':
                        host_cache_folder=  host_cache_folder[:-1]
                    out_dir_docker = out_dir.replace(cache_folder, host_cache_folder)

                colmap_container = UtilsContainers('docker', 'colmap/colmap',  out_dir_docker)
                # colmap_container = UtilsContainers('singularity', dir_path + '/colmap.sif', out_dir)
            colmap = Colmap(colmap_container)       

            # init db for keypoints 
            if not chunk.node.database.value:
                colmap.prepare_database(out_dir + '/database.db', '/data/database.db')
            else:
                shutil.copy2(chunk.node.database.value, chunk.node.output.value)

            # COLMAP detector
            if chunk.node.algorithm.value == 'SIFT':
                chunk.logger.info('COLMAP --> compute SIFT features')
                colmap.extract_features('/data/database.db', '/data', chunk.node.cameraModel.value)           # COLMAP feature extractor
            
            # remove images in cache
            if chunk.node.removeImages.value:
                holo_io.remove_images_from_cache(out_dir)

            chunk.logger.info('KeypointsDetector done.')
            chunk.logger.info("DELETING IMAGES")
            # Remove images
            for f in os.listdir(str(out_dir)):
                full_f = os.path.join(out_dir,f)
                if os.path.isfile(f) and ("jpg" in f or "png" in f):
                    os.remove(full_f )
                elif os.path.isdir(full_f):
                    files = os.listdir(full_f)
                    for file in files:
                        if os.path.isfile(os.path.join(full_f,file)) and ("jpg" in file or "png" in file):
                            os.remove(os.path.join(full_f, file))
        
        except AssertionError as err:
            chunk.logger.error('Error in keyframe selector: ' + err)
        finally:
            chunk.logManager.end()
