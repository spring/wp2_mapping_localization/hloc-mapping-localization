from pathlib import Path
from pprint import pformat
import argparse
import torch 
import h5py
import numpy as np 

from hloc import extract_features, match_features, extractors, matchers
from hloc import pairs_from_covisibility, pairs_from_retrieval
from hloc import colmap_from_nvm, triangulation, localize_sfm
from hloc.utils.read_write_model import read_images_binary
from hloc.utils.io import list_h5_names
from hloc.utils.base_model import dynamic_load


parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=Path, default='/data/input',
                    help='Path to the dataset, default: %(default)s')
parser.add_argument('--outputs', type=Path, default='/data/output',
                    help='Path to the output directory, default: %(default)s')
parser.add_argument('--num_covis', type=int, default=20,
                    help='Number of image pairs for SfM, default: %(default)s')
parser.add_argument('--num_loc', type=int, default=50,
                    help='Number of image pairs for loc, default: %(default)s')
args = parser.parse_args()

# Setup the paths
dataset = args.dataset
images = dataset
sift_sfm = dataset   # from which we extract the reference poses

outputs = args.outputs  # where everything will be saved
reference_sfm = outputs / 'sfm_superpoint+superglue'  # the SfM model we will build
sfm_pairs = outputs / f'pairs-db-covis{args.num_covis}.txt'  # top-k most covisible in SIFT model
results = outputs / f'Aachen_hloc_superpoint+superglue_netvlad{args.num_loc}.txt'


class Hloc():
    # torch.backends.cudnn.enabled = False
    def __init__(self, map_path):
        map_path = Path(map_path)        
        self.retrieval_conf = extract_features.confs['netvlad']
        self.feature_conf = extract_features.confs['superpoint_aachen']
        self.matcher_conf = match_features.confs['superglue']

        # database features and descriptors
        self.reference_sfm = map_path / 'sfm_superpoint+superglue'
        self.db_feature_path = map_path / 'feats-superpoint-n4096-r1024.h5'
        self.db_descriptors_path = map_path / 'global-feats-netvlad.h5'
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.db_desc = None
        
        self.feature_model = self.get_model(self.feature_conf)
        self.retrieval_model = self.get_model(self.retrieval_conf)
        self.matcher_model = self.get_model(self.matcher_conf, match=True)
        self.models = (self.feature_model, self.retrieval_model, self.matcher_model)

    def get_model(self, conf, match=False):
        if not match:
            Model = dynamic_load(extractors, conf['model']['name'])
        else:
            Model = dynamic_load(matchers, conf['model']['name'])

        model = Model(conf['model']).eval().to(self.device)
        return model

    def get_db(self):
        db_model = self.reference_sfm
        db_descriptors = self.db_descriptors_path

        if isinstance(db_descriptors, (Path, str)):
            db_descriptors = [db_descriptors]
        name2db = {n: i for i, p in enumerate(db_descriptors)
                   for n in list_h5_names(p)}

        images = read_images_binary(db_model / 'images.bin')
        db_names = [i.name for i in images.values()]
        if len(db_names) == 0:
            raise ValueError('Could not find any database image.')

        def get_descriptors(names, path, name2idx=None, key='global_descriptor'):
            device = 'cuda' if torch.cuda.is_available() else 'cpu'

            if name2idx is None:
                with h5py.File(str(path), 'r') as fd:
                    desc = [fd[n][key].__array__() for n in names]
            else:
                desc = []
                for n in names:
                    with h5py.File(str(path[name2idx[n]]), 'r') as fd:
                        desc.append(fd[n][key].__array__())
            return torch.from_numpy(np.stack(desc, 0)).to(device).float()
        db_desc = get_descriptors(db_names, db_descriptors, name2db)

        return (db_desc, db_names)

# list the standard configurations available
print(f'Configs for feature extractors:\n{pformat(extract_features.confs)}')
print(f'Configs for feature matchers:\n{pformat(match_features.confs)}')

hloc_obj = Hloc(outputs)

# prepare the map
features = extract_features.main(hloc_obj.feature_conf, hloc_obj.feature_model, images, outputs)

pairs_from_covisibility.main(
    sift_sfm, sfm_pairs, num_matched=args.num_covis)

sfm_matches = match_features.main(hloc_obj.matcher_conf,hloc_obj.matcher_model, sfm_pairs, hloc_obj.feature_conf['output'],outputs)


triangulation.main(
    reference_sfm,
    sift_sfm,
    images,
    sfm_pairs,
    features,
    sfm_matches,
    colmap_path='colmap')

global_descriptors = extract_features.main(hloc_obj.retrieval_conf, hloc_obj.retrieval_model, images, outputs)